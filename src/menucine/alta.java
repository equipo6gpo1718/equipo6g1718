/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menucine;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jimmy
 */
public class alta extends JFrame implements ActionListener{

    /**
     *
     */
    public alta(){
        marco();
    }

    private void marco() {
        this.setSize(500,400);
        this.setTitle("Alta de empleado");
        this.setLocation(200,200);
        this.setVisible(true);
        
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        JPanel panelOeste = new JPanel();
        
        //Layout Managers a utilzar
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelCentro.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout(3, 0);
        panelEste.setLayout(gl2);
       
        GridLayout gl3 = new GridLayout(3, 0);
        panelOeste.setLayout(gl3);
        
        //creamos componetes para el panel norte
        JLabel lglTitulo = new JLabel("Alta para Empleado,"
                + " Seleccione area de Empleado");
        
        //creamos componetes para el panel centro
        JLabel lbl1 = new JLabel("Nombre:");
        JTextField tf1 = new  JTextField(25);
        JLabel lbl2 = new JLabel("CURP:");
        JTextField tf2 = new  JTextField(25);
        JLabel lbl3 = new JLabel("Direccion:");
        JTextField tf3 = new  JTextField(25);
        JLabel lbl4 = new JLabel("Telefono:");
        JTextField tf4 = new  JTextField(25);
        
        String[] lista = {"Boca del rio","Veracruz"};
        JLabel lbl5 = new JLabel("sucursal:");
        JComboBox cb1 = new JComboBox(lista);
        
        String[] lista2 = {"Boletos","Dulceria","Seguridad","Limpieza"};
        JLabel lbl6 = new JLabel("Puesto:");
        JComboBox cb2 = new JComboBox(lista2);
        
        //componentes para panel sur
        JButton bs1 = new JButton();
        JButton bs2 = new JButton();
        JButton bs3 = new JButton();
        bs1.setText("Aceptar");
        bs2.setText("Cancelar");
        bs3.setText("Salir");
        bs3.setName("salir");
        //componentes para panel oeste
        
        
        
        //componentes para el panel este
        
        
        //agregados de componentes a paneles
        
        panelNorte.add(lglTitulo);
        
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        
        gbc1.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1,gbc1);
        gbc1.gridy = 1;
        gbc1.gridwidth = 5;
        panelCentro.add(tf1,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 2;
        panelCentro.add(lbl2,gbc1);
        gbc1.gridy = 3;
        gbc1.gridwidth = 5;
        panelCentro.add(tf2,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 4;
        panelCentro.add(lbl3,gbc1);
        gbc1.gridy = 5;
        gbc1.gridwidth = 5;
        panelCentro.add(tf3,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=6;
        panelCentro.add(lbl4,gbc1);
        gbc1.gridwidth=5;
        gbc1.gridy=7;
        panelCentro.add(tf4,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=8;
        panelCentro.add(lbl5,gbc1);
        gbc1.gridwidth=2;
        gbc1.gridy=9;
        panelCentro.add(cb1,gbc1);
        gbc1.gridwidth=2;
        gbc1.gridy=10;
        panelCentro.add(lbl6,gbc1);
        gbc1.gridwidth=5;
        gbc1.gridy=12;
        panelCentro.add(cb2,gbc1);
        gbc1.gridwidth=2;
        gbc1.gridy=13;
        
        
        panelSur.add(bs1);
        panelSur.add(bs2);
        panelSur.add(bs3);
        
        bs3.addActionListener(this);
        
        
        
        //Integrar paneles a panelmayor
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        
        //asociar el panel mayor a la forma o ventana
        this.add(panelMayor);
        
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton origen = (JButton)e.getSource();
        switch(origen.getName()){
            case"salir":
                this.setVisible(false);
                break;
        }
    }
    
}
