/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menucine;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Angel
 */
public class Promociones extends JFrame {   
    
    public Promociones(){
    marco();
    
    }

    private void marco() {
        this.setSize(400,400);
        this.setTitle("Promociones");
        this.setLocation(200,200);
        this.setVisible(true);
        
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        JPanel panelOeste = new JPanel();
  
        
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridLayout gl1 = new GridLayout(12,5,8,8);
        panelCentro.setLayout(gl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout();
        panelEste.setLayout(gl2);
       
        GridLayout gl3 = new GridLayout();
        panelOeste.setLayout(gl3);
        
        //creamos componetes para el panel norte
        JLabel lglTitulo = new JLabel("PROMOCIONES");
        
             //SUR 
        JButton bs1 = new JButton();
        bs1.setText("Salir");
        JButton bs2 = new JButton();
        bs2.setText("Regresar");
         
        // ESTE
        JButton be1 = new JButton();
        be1.setText("CLIENTE: 2X1 L-V");
        JButton be2 = new JButton();
        be2.setText("MIERCOLES 2X1");
        
          // ESTE
        JButton bo1 = new JButton();
        bo1.setText("3x2");
        JButton bo2 = new JButton();
        bo2.setText("DIA CUMPLEAÑOS");
        
        panelNorte.add(lglTitulo);
        panelSur.add(bs1);
        panelSur.add(bs2);
        panelEste.add(be1);
        panelEste.add(be2);
        panelOeste.add(bo1);
        panelOeste.add(bo2);
        
         panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        
        //asociar el panel mayor a la forma o ventana
        this.add(panelMayor);
        
        this.setVisible(true);
        
    }
    
}
