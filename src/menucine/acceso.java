/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menucine;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jimmy
 */
public class acceso extends JFrame implements ActionListener{
    public acceso(){
        marco();
    }

    private void marco() {
        
        this.setSize(380,250);
        this.setTitle("Acceso de Empleado");
        this.setLocation(200,200);
        this.setVisible(true);
        
        
        JPanel panelMayor = new JPanel(new BorderLayout());
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel sur=new JPanel();
        
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panel1.setLayout(fl1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panel2.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        sur.setLayout(fl2);
        
        JLabel lglTitulo = new JLabel("Coloque su nombre de usuario y contraseña para poder acceder");
        
        JLabel lbl1 = new JLabel("Nom.de Usuario");
        JTextField tf1 = new JTextField(15);
        JLabel lbl2 = new JLabel("Contraseña");
        JTextField tf2 = new JTextField(15);
        JButton b1 = new JButton("Aceptar");
        JButton b2 = new JButton("Salir");
        b2.setName("salir");
                
        panel1.add(lglTitulo);
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        
        gbc1.anchor = GridBagConstraints.WEST;
        panel2.add(lbl1,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panel2.add(tf1,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=2;
        panel2.add(lbl2,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panel2.add(tf2,gbc1);
        gbc1.gridwidth=1;
        gbc1.gridy=4;
        
        sur.add(b1);
        sur.add(b2);
        
        b2.addActionListener(this);
        
        panelMayor.add(panel1, BorderLayout.NORTH);
        panelMayor.add(panel2, BorderLayout.CENTER);
        panelMayor.add(sur, BorderLayout.SOUTH);
        
        
        this.add(panelMayor);
        this.setVisible(true);
        
    }

    public void actionPerformed(ActionEvent e) {
       JButton origen = (JButton)e.getSource();
        switch(origen.getName()){
            case"salir":
                this.setVisible(false);
                break;
        }
    }

    

    
    
}
