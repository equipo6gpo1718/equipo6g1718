/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menucine;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Angel
 */
public class Extra extends JFrame{
    public Extra(){
        this.setSize(400,400);
        this.setTitle("Extras");
        this.setLocation(200,200);
        this.setVisible(true);
    
             JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        JPanel panelOeste = new JPanel();
        
        //Layout Managers a utilzar
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridLayout gl1 = new GridLayout(12,5,8,8);
        panelCentro.setLayout(gl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout(12,5,8,8);
        panelEste.setLayout(gl2);
       
        GridLayout gl3 = new GridLayout(12,5,8,8);
        panelOeste.setLayout(gl3);
        
        //creamos componetes para el panel norte
        JLabel lglTitulo = new JLabel(" Extras ");
        
        //creamos componetes para el panel centro
        JLabel lbl1 = new JLabel("  ");
        JLabel lbl12 = new JLabel(" ");
        JLabel lbl13 = new JLabel(" ");
        JLabel lbl14 = new JLabel(" ");
        JLabel lbl15 = new JLabel(" ");
        JLabel lbl16 = new JLabel(" ");
        JLabel lbl17 = new JLabel(" ");
        JLabel lbl18 = new JLabel(" ");
        //componentes para panel sur
        JButton bs1 = new JButton();
        
        bs1.setText("Salir");
        
        
        //componentes para panel oeste
        JLabel lbl2 = new JLabel(" ");
        
        JLabel lbl22 = new JLabel(" ");
        JLabel lbl23 = new JLabel(" ");
        JLabel lbl24 = new JLabel(" ");
        JLabel lbl25 = new JLabel(" ");
        JLabel lbl26 = new JLabel(" ");
        JLabel lbl27 = new JLabel(" ");
        JLabel lbl28 = new JLabel(" ");
        //componentes para el panel este
        JLabel lbl3 = new JLabel(" ");
        
        JLabel lbl32 = new JLabel(" ");
        JLabel lbl33 = new JLabel(" ");
        JLabel lbl34 = new JLabel(" ");
        JLabel lbl35 = new JLabel(" ");
        JLabel lbl36 = new JLabel(" ");
        JLabel lbl37 = new JLabel(" ");
        JLabel lbl38 = new JLabel(" ");
        
        //agregados de componentes a paneles
        
        panelNorte.add(lglTitulo);
        
        
        panelCentro.add(lbl1);
        panelCentro.add(lbl12);
        panelCentro.add(lbl13);
        panelCentro.add(lbl14);
        panelCentro.add(lbl15);
        panelCentro.add(lbl16);
        panelCentro.add(lbl17);
        panelCentro.add(lbl18);
        
        
        panelSur.add(bs1);
        
        
        panelEste.add(lbl2);
        panelEste.add(lbl22);
        panelEste.add(lbl23);
        panelEste.add(lbl24);
        panelEste.add(lbl25);
        panelEste.add(lbl26);
        panelEste.add(lbl27);
        panelEste.add(lbl28);
        
        panelOeste.add(lbl3);
        panelOeste.add(lbl32);
        panelOeste.add(lbl33);
        panelOeste.add(lbl34);
        panelOeste.add(lbl35);
        panelOeste.add(lbl36);
        panelOeste.add(lbl37);
        panelOeste.add(lbl38);
        
        
        
        
        //Integrar paneles a panelmayor
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        
        //asociar el panel mayor a la forma o ventana
        this.add(panelMayor);
        
//        this.setVisible(true);
    
    }
}
