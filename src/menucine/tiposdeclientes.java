/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menucine;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 *
 * @author jimmy
 */
public class tiposdeclientes extends JFrame implements ActionListener{
     public tiposdeclientes(){
        marco();
    }

    private void marco() {
        this.setSize(680,380);
        this.setTitle("Tipo de Clientes");
        this.setLocation(200,200);
        this.setVisible(true);
        
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        JPanel panelOeste = new JPanel();
        
        //Layout Managers a utilzar
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridLayout gl1 = new GridLayout(12,5,8,8);
        panelCentro.setLayout(gl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout(12,5,8,8);
        panelEste.setLayout(gl2);
       
        GridLayout gl3 = new GridLayout(12,5,8,8);
        panelOeste.setLayout(gl3);
        
        //creamos componetes para el panel norte
        JLabel lglTitulo = new JLabel("Tipos de clientes");
        
        //creamos componetes para el panel centro
        JLabel lbl1 = new JLabel(".   Medio:");
        JLabel lbl12 = new JLabel(".   El cliente medio y sus beneficios son:");
        JLabel lbl13 = new JLabel(".   -boletos 2x1 de lunes a viernes");
        JLabel lbl14 = new JLabel(".   -refil de refresco y palomitas x $15 c/u");
        JLabel lbl15 = new JLabel(".   -acumula 5% de compra");
        JLabel lbl16 = new JLabel(".   -combo de cumpleaños");
        JLabel lbl17 = new JLabel(".   -invitaciones a premieres");
        JLabel lbl18 = new JLabel(".   -promociones en dulceria");
        //componentes para panel sur
        JButton bs1 = new JButton();
        
        bs1.setText("Salir");
        bs1.setName("salir");
        
        //componentes para panel oeste
        JLabel lbl2 = new JLabel(".   premium:");
        
        JLabel lbl22 = new JLabel(".   El cliente premium y sus beneficios son:");
        JLabel lbl23 = new JLabel(".   -boletos 2x1 toda la semana");
        JLabel lbl24 = new JLabel(".   -refil de refresco y palomitas x $10 c/u");
        JLabel lbl25 = new JLabel(".   -acumula 7% de compra");
        JLabel lbl26 = new JLabel(".   -combo de cumpleaños");
        JLabel lbl27 = new JLabel(".   -invitaciones a premieres");
        JLabel lbl28 = new JLabel(".   -promociones en dulceria");
        //componentes para el panel este
        JLabel lbl3 = new JLabel("basico:");
        
        JLabel lbl32 = new JLabel("El cliente basico y sus beneficios son:");
        JLabel lbl33 = new JLabel("-boletos 2x1 de lunes a miercoles");
        JLabel lbl34 = new JLabel("-refil de refresco y palomitas x $20 c/u");
        JLabel lbl35 = new JLabel("-acumula 4% de compra");
        JLabel lbl36 = new JLabel("-combo de cumpleaños");
        JLabel lbl37 = new JLabel("-invitaciones a premieres");
        JLabel lbl38 = new JLabel("-promociones en dulceria");
        
        //agregados de componentes a paneles
        
        panelNorte.add(lglTitulo);
        
        
        panelCentro.add(lbl1);
        panelCentro.add(lbl12);
        panelCentro.add(lbl13);
        panelCentro.add(lbl14);
        panelCentro.add(lbl15);
        panelCentro.add(lbl16);
        panelCentro.add(lbl17);
        panelCentro.add(lbl18);
        
        
        panelSur.add(bs1);
        bs1.addActionListener(this);
        
        panelEste.add(lbl2);
        panelEste.add(lbl22);
        panelEste.add(lbl23);
        panelEste.add(lbl24);
        panelEste.add(lbl25);
        panelEste.add(lbl26);
        panelEste.add(lbl27);
        panelEste.add(lbl28);
        
        panelOeste.add(lbl3);
        panelOeste.add(lbl32);
        panelOeste.add(lbl33);
        panelOeste.add(lbl34);
        panelOeste.add(lbl35);
        panelOeste.add(lbl36);
        panelOeste.add(lbl37);
        panelOeste.add(lbl38);
        
        
        
        
        //Integrar paneles a panelmayor
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        
        //asociar el panel mayor a la forma o ventana
        this.add(panelMayor);
        
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton origen = (JButton)e.getSource();
        switch(origen.getName()){
            case"salir":
                this.setVisible(false);
                break;
        }
    }

   
    
}
